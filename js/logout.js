$(document).ready(function () {
    $('#logout-button').click(function () {
        logout();
        $('.loadingScreen').show();
        $('input[required], select[required]').removeAttr('required');
        setTimeout(function()
        {
            window.location.href = "login.html";
        }, 1500);
    });
});

var logout = function()
{
    $.session.clear();
};