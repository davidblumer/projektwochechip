$(document).ready(function () {
    var data =
    {
        "disciplines": [
            {
                'Skifahren': '40€'
            },
            {
                'Schlittenfahren': '25€'
            },
            {
                'Schwimmen': '30€'
            },
            {
                'Schlittschuhlaufen': '5€'
            }
        ]

    };

    $('#disciplineInput').change(function () {
        var selectedOption = $(this).val();

        $.each(data.disciplines, function (i) {
            $.each(data.disciplines[i], function(key, val){
               if(key == selectedOption)
               {
                   $('.price-panel').val(val);
               }
            });
        });

        if(selectedOption != 'Skifahren')
        {
            $('.level').hide();
        }
        else
        {
            $('.level').show();
        }

    });
});