$(document).ready(function () {
    $('#registrateButton').click(function (e) {


        writeFormToPDF();
        //windowOpener('formToPdf.html', 'Registrierungsbestätigung');
        e.preventDefault();
    });
});

function windowOpener(url, name, args) {


    popupWin = window.open(url, name, args);

    popupWin.focus();
}


var writeFormToPDF = function()
{
    var firstnameInput = $('#firstnameInput').val();
    var lastnameInput = $('#lastnameInput').val();
    var classInput = $('#classInput').val();
    var priceInput = $('#priceInput').val();
    var disciplineInput = $('#disciplineInput').val();
    var levelInput = $('#levelInput').val();

    $('#firstname').text(firstnameInput);
    $('#lastname').text(lastnameInput);
    $('#class').text(classInput);
    $('#price').text(priceInput);
    $('#discipline').text(disciplineInput);
    $('#level').text(levelInput);
};