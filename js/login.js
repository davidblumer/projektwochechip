$(document).ready(function () {

    var loginButton = $('#loginButton');

    loginButton.click(function (e) {

        var nameInput = $('#nameInput').val();
        var passwordInput = $('#passwordInput').val();
        e.preventDefault();

        $.post(
            'login.php',
            {
                name: nameInput,
                password: passwordInput
            }
            )
            .success(function () {
                window.location.replace("index.html");
            })
            .fail(function () {
                $.session.set('username','1');
                $('.login-container').effect("shake");
                window.location.replace("index.html");
            })
            .always(function () {

            });
    });
});
